// 
var minTemp = -40;
var maxTemp = 60;
var tempScale = '°C';
// 
// 
var tempLimit = 45;
// 

// 
var minPres = 910;
var maxPres = 1040;
var presScale = 'hPa';
// 

// 
var minWind = 0;
var maxWind = 130;
var windScale = 'km/h';
// 
// 
var windLimit = 80;
// 

// 
// 
var mainTitle = 'Wetterstation';
var tempTitle = 'Temperatur';
var presTitle = 'Luftdruck';
var tempWarning = 'Extreme Hitze';
var windTitle = 'Windgeschwindigkeit';
var windWarning = 'Sturmwarnung';
// 
